#include <stdio.h>
#include <stdlib.h>
#include <string.h>

//DECLARATION DES CONSTANTES
#define TAILLETAB 10

//DECLARATION DES FONCTIONS
int menu();
int cryptage();
int decryptage();
int lecture_fic(char []);
int ecriture(char [], char []);
void suppr_fic(char []);

int main()
{
//DECLARATION DES TABLEAUX ET VARIABLES
    int choixMenu;
    char modif_source[TAILLETAB+1]={};
    char modif_cle[TAILLETAB+1]={};
    int temp = 0;

    do
    {
        choixMenu = menu();

        switch (choixMenu)
        {
        case 1: //ECRIRE DANS SOURCE.TXT
            do
            {
                printf("\nSaisir le texte a cypter, %d caracteres max : ", TAILLETAB);
                fflush(stdin);
                gets(modif_source);
                int lenght_modif_source = strlen(modif_source);
                printf("La taille est du texte saisi est : %d\n",lenght_modif_source);

                if (lenght_modif_source > TAILLETAB)
                {
                    printf("Saisir moins de %d caracteres.\n", TAILLETAB);
                    temp = 0;
                }else{
                    printf("Nombre de caracteres valide.\n");
                    temp = 1;

                    ecriture(modif_source, "source.txt");
                }
            }while(temp == 0);

        break;

        case 2: //ECRIRE DANS PEROQ.DEF
            do
            {
                printf("\nSaisir la cle de cryptage, %d caracteres max : ", TAILLETAB);
                fflush(stdin);
                gets(modif_cle);
                int lenght_modif_cle = strlen(modif_cle);
                printf("La taille est du texte saisi est : %d\n",lenght_modif_cle);

                if (lenght_modif_cle > TAILLETAB)
                {
                    printf("Saisir moins de %d caracteres.\n", TAILLETAB);
                    temp = 0;
                }else{
                    printf("Nombre de caracteres valide.\n");
                    temp = 1;

                    ecriture(modif_cle, "peroq.def");
                }
            }while(temp == 0);

        break;

        case 3: //CRYPTER LE TEXTE
            cryptage();

        break;

        case 4: //DECRYPTER LE TEXTE
            decryptage();
        break;

        }
    }while (choixMenu != 5);

    return 0;
}

//MENU
int menu ()
{
    int choix;
    do
    {
    printf("Que voulez-vous faire ?");
    printf("\n1-Modifier le texte a crypter");
    printf("\n2-Modifier la cle de cryptage");
    printf("\n3-Crypter un texte");
    printf("\n4-Decrypter un texte");
    printf("\n5-Quitter\n");
    fflush(stdin);
    scanf("%d", &choix);

    if (choix < 1 || choix > 5)
        printf("Mauvaise entree, choisissez un chiffre entre 1 et 5.\n");

    }while (choix < 1 || choix > 5);

    return choix;
}

//CRYPTAGE
int cryptage()
{
    char crypt[TAILLETAB+1]={};
    char cle[TAILLETAB+1]={};
    char lettre;
    int compteur=0;

///////////////////////////////////////////////////
//FICHIER SOURCE.TXT
    //OUVERTURE DU FICHIER SOURCE.TXT
    FILE *fp_source = NULL;
    fp_source = fopen("source.txt", "r");

    if (fp_source == NULL)
    {
        printf("\nErreur lors de l'ouverture du fichier source.txt.\n");
        printf("---------\nRetour au Menu\n\n");
        return EXIT_FAILURE;
    }

    //LECTURE DU FICHIER SOURCE.TXT ET ECRITURE DANS LE TABLEAU ASSOCIE
    fread(&lettre, sizeof(lettre), 1, fp_source);
    if (feof(fp_source))
    {
        printf("\nFichier vide.\n");
    }

    while(!feof(fp_source))
    {
        crypt[compteur] = lettre;
        compteur++;

        fread(&lettre, sizeof(lettre), 1, fp_source);
        if (feof(fp_source))
        {
            compteur=0;
        }
    }

    //FERMETURE DU FICHIER SOURCE.TXT
    int ret_source = fclose(fp_source);
    if (ret_source != 0)
    {
        printf("\nErreur lors de la fermeture du fichier source.txt.\n");
        return EXIT_FAILURE;
    }

///////////////////////////////////////////////////
// FICHIER PEROQ.DEF

    //OUVERTURE DU FICHIER PEROQ.DEF
    FILE *fp_cle = NULL;
    fp_cle = fopen("peroq.def", "r");

    if (fp_cle == NULL)
    {
        printf("\nErreur lors de l'ouverture du fichier peroq.def.\n");
        printf("---------\nRetour au Menu\n\n");
        return EXIT_FAILURE;
    }

    //LECTURE DU FICHIER PEROQ.DEF ET ECRITURE DANS LE TABLEAU ASSOCIE
    fread(&lettre, sizeof(lettre), 1, fp_cle);
    if (feof(fp_cle))
    {
        printf("\n Fichier vide.\n");
    }

    while(!feof(fp_cle))
    {
        cle[compteur] = lettre;
        compteur++;

        fread(&lettre, sizeof(lettre), 1, fp_cle);
        if (feof(fp_cle))
        {
            compteur=0;
        }
    }

    //FERMETURE DU FICHIER PEROQ.DEF
    int ret_key = fclose(fp_cle);
    if (ret_key != 0)
    {
        printf("\nErreur lors de la fermeture du fichier peroq.def.\n");
        return EXIT_FAILURE;
    }

///////////////////////////////////////////////////
//CRYPTAGE
    //Récupération de la taille des tableaux
    int length_source = strlen(crypt);
    int lenght_cle = strlen(cle);

    //Ouverture du fichier dest.ctr pour le cryptage
    FILE *fp = NULL;
    fp = fopen("dest.ctr", "w+");

    if (fp == NULL)
    {
        printf("\nErreur lors de l'ouverture du fichier dest.ctr.\n");
        return EXIT_FAILURE;
    }


    //Cryptage de la chaine de caractère et écriture dans dest.ctr
    for (int i=0; i<length_source; i++)
    {
        crypt[i] -= cle[(i%lenght_cle)];
    }
    fwrite(crypt, strlen(crypt), 1, fp);

    printf("\nCryptage termine.\n\n");

    //Fermeture du fichier dest.ctr
    int ret = fclose(fp);
    if (ret != 0)
    {
        printf("\nErreur lors de la fermeture du fichier dest.ctr.\n");
        return EXIT_FAILURE;
    }

    lecture_fic("dest.ctr");

///////////////////////////////////////////////////
//SUPPRESSION DU FICHIER SOURCE.TXT
    suppr_fic("source.txt");
    printf("---------\nRetour au Menu\n\n");

    return 0;
}

//FONCTION POUR ECRIRE DANS UN FICHIER
int ecriture(char modif_fic[], char fichier[])
{
    //Ouverture du fichier source.txt pour la modification
    FILE *fp = NULL;
    fp = fopen(fichier, "w+");

    if (fp == NULL)
    {
        printf("\nErreur d'ouverture du fichier.\n");
        printf("---------\nRetour au Menu\n\n");
        return EXIT_FAILURE;
    }

    fwrite(modif_fic, strlen(modif_fic), 1, fp);

    printf("\nEcriture dans le fichier %s terminee.\n\n", fichier);

    //Fermeture du fichier source.txt
    int ret = fclose(fp);
    if (ret != 0)
    {
        printf("\nErreur lors de la fermeture du fichier.\n");
        return EXIT_FAILURE;
    }

    return 0;
}

//FONCTION POUR LIRE UN FICHIER
int lecture_fic(char fichier[])
{
    char lettre;

//OUVERTURE DU FICHIER DEST.CTR POUR LIRE LE RESULTAT DU CRYPTAGE
    FILE *fp = NULL;
    fp = fopen(fichier, "r");

    if (fp == NULL)
    {
        printf("\nErreur lors de l'ouverture du fichier.\n");
        printf("---------\nRetour au Menu\n\n");
        return EXIT_FAILURE;
    }

    //Affiche le contenu du fichier dest.ctr
    printf("Contenu du fichier %s : \n", fichier);
    fread(&lettre, sizeof(lettre), 1, fp);
    if (feof(fp))
    {
        printf("\nFichier vide.\n");
    }

    while(!feof(fp))
    {
        printf("%c", lettre);
        fread(&lettre, sizeof(lettre), 1, fp);
        if (feof(fp))
        {
            printf("\n\nFin du fichier.\n");
        }
    }

    int ret = fclose(fp);
    if (ret != 0)
    {
        printf("\nErreur lors de la fermeture du fichier.\n");
        return EXIT_FAILURE;
    }


    return 0;
}

//FONCTION POUR SUPPRIMER UN FICHIER
void suppr_fic(char fichier[])
{
    if (remove(fichier) == 0)
    {
        printf("\n%s avec succes.\n\n", fichier);
    }else{
        printf("\nErreur lors de la suppression du fichier.\n");
    }
}

//DECRYTAGE
int decryptage()
{
    char crypt[TAILLETAB+1]={};
    char cle[TAILLETAB+1]={};
    char lettre;
    int compteur=0;

///////////////////////////////////////////////////
//FICHIER DEST.CTR
    //OUVERTURE DU FICHIER DEST.CTR
    FILE *fp_source = NULL;
    fp_source = fopen("dest.ctr", "r");

    if (fp_source == NULL)
    {
        printf("\nErreur lors de l'ouverture du fichier source.txt.\n");
        printf("---------\nRetour au Menu\n\n");
        return EXIT_FAILURE;
    }

    //LECTURE DU FICHIER DEST.CTR ET ECRITURE DANS LE TABLEAU ASSOCIE
    fread(&lettre, sizeof(lettre), 1, fp_source);
    if (feof(fp_source))
    {
        printf("\nFichier vide.\n");
    }

    while(!feof(fp_source))
    {
        crypt[compteur] = lettre;
        compteur++;

        fread(&lettre, sizeof(lettre), 1, fp_source);
        if (feof(fp_source))
        {
            compteur=0;
        }
    }

    //FERMETURE DU FICHIER DEST.CTR
    int ret_source = fclose(fp_source);
    if (ret_source != 0)
    {
        printf("\nErreur lors de la fermeture du fichier source.txt.\n");
        return EXIT_FAILURE;
    }

///////////////////////////////////////////////////
// FICHIER PEROQ.DEF

    //OUVERTURE DU FICHIER PEROQ.DEF
    FILE *fp_cle = NULL;
    fp_cle = fopen("peroq.def", "r");

    if (fp_cle == NULL)
    {
        printf("\nErreur lors de l'ouverture du fichier peroq.def.\n");
        printf("---------\nRetour au Menu\n\n");
        return EXIT_FAILURE;
    }

    //LECTURE DU FICHIER PEROQ.DEF ET ECRITURE DANS LE TABLEAU ASSOCIE
    fread(&lettre, sizeof(lettre), 1, fp_cle);
    if (feof(fp_cle))
    {
        printf("\n Fichier vide.\n");
    }

    while(!feof(fp_cle))
    {
        cle[compteur] = lettre;
        compteur++;

        fread(&lettre, sizeof(lettre), 1, fp_cle);
        if (feof(fp_cle))
        {
            compteur=0;
        }
    }

    //FERMETURE DU FICHIER PEROQ.DEF
    int ret_key = fclose(fp_cle);
    if (ret_key != 0)
    {
        printf("\nErreur lors de la fermeture du fichier peroq.def.\n");
        return EXIT_FAILURE;
    }

///////////////////////////////////////////////////
//DECRYPTAGE
    //Récupération de la taille des tableaux
    int length_source = strlen(crypt);
    int lenght_cle = strlen(cle);

    //Ouverture du fichier dest.ctr pour le décryptage
    FILE *fp = NULL;
    fp = fopen("dest.ctr", "w+");

    if (fp == NULL)
    {
        printf("\nErreur lors de l'ouverture du fichier dest.ctr.\n");
        return EXIT_FAILURE;
    }


    //Décryptage de la chaine de caractère et écriture dans dest.ctr
    for (int i=0; i<length_source; i++)
    {
        crypt[i] += cle[(i%lenght_cle)];
    }
    fwrite(crypt, strlen(crypt), 1, fp);

    printf("\nDecryptage termine.\n\n");

    //Fermeture du fichier dest.ctr
    int ret = fclose(fp);
    if (ret != 0)
    {
        printf("\nErreur lors de la fermeture du fichier dest.ctr.\n");
        return EXIT_FAILURE;
    }

    lecture_fic("dest.ctr");

    return 0;
}

